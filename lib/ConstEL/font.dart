import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vaionapp/ConstEL/color.dart';

class ConstTextStyle{
  static styleinter({
    double? fontsize,
    FontWeight? fontWeight,
    Color? color,
    double? height,
  }){
    return GoogleFonts.inter(
      fontSize: fontsize??24,
      fontWeight: fontWeight?? FontWeight.bold,
      color: color??warnaputih,
      height: height??1,
    );
  }

  static stylepoppin({
    double? fontsize,
    FontWeight? fontWeight,
    Color? color,
    double? height,
  }){
    return GoogleFonts.poppins(
      fontSize: fontsize??24,
      fontWeight: fontWeight?? FontWeight.bold,
      color: color??warnaputih,
      height: height??1,
    );
  }
}