import 'package:get/get.dart';
import 'package:vaionapp/feature/modele/favorit_model.dart';

class FavoriteController extends GetxController {

  final RxBool _isGrid = false.obs;
  RxBool get isGrid => _isGrid;

  void tooggleIsGrid(){
    _isGrid.toggle();
  }

  void addtoFavorite(int index){
    late String title;
    late String message;
    final favorite = favoriteList[index].copywith(
      isFavorit: !favoriteList[index].isFavorit,
    );
    favoriteList[index] = favorite;
    if (favorite.isFavorit){
      title = "Berhasil Ditambahkan";
      message = '"${favorite.menutitle}" Ditambahkan ke Favorit';
    } else {
      title = "Berhasil Dihapus";
      message = '"${favorite.menutitle}"" Dihapus dari Favorit';
    }
    Get.showSnackbar(
      GetSnackBar(
        title: title,
        message: message,
        duration: const Duration(seconds: 3),
      )
    );
  }

  final RxList<FavoritModel> favoriteList = [
    FavoritModel(
      restoName: 'Warung haji', 
      menutitle: 'Burger Tikus', 
      alamat: 'Jl. Sempoyongan', 
      isFavorit: false, 
      harga: '28',
      imageUrl: 'assets/images/burger.png',
      rating: '4.5'
    ),
    FavoritModel(
      restoName: 'Warung hafiz', 
      menutitle: 'Basic Icecream', 
      alamat: 'Jl. Sempojangan', 
      isFavorit: false, 
      harga: '15',
      imageUrl: 'assets/images/eskrim1.jpg',
      rating: '4.2'
      ),
    FavoritModel(
      restoName: 'Warung haji', 
      menutitle: 'Pizza pineapple', 
      alamat: 'Jl. Sempoyongan', 
      isFavorit: false, 
      harga: '52',
      imageUrl: 'assets/images/pizza.png',
      rating: '4.7'
      ),
    FavoritModel(
      restoName: 'Warung jainudin', 
      menutitle: 'Ayam Presto', 
      alamat: 'Jl. jatidiri', 
      isFavorit: false, 
      harga: '45',
      imageUrl: 'assets/images/ayampresto.jpg',
      rating: '4.4'
    ),
  ].obs;
}