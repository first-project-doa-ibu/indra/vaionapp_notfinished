import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Favorite/Controller/Favorit_controller.dart';

class GridCard extends StatelessWidget {
  const GridCard({super.key, required this.imageUrl, required this.harga, required this.rating, required this.restoName, required this.alamat, required this.menutitle, required this.isFavorit, required this.index, required this.favoriteController});

  final String imageUrl;
  final String harga;
  final String rating;
  final String restoName;
  final String alamat;
  final String menutitle;
  final bool isFavorit;
  final int index;
  final FavoriteController favoriteController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: warnakotak
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 100,
            child: Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(imageUrl,
                  width: 150,
                  height: 100,
                  fit: BoxFit.cover,
                  ),
                ),
                Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 11,
                        vertical: 3
                      ),
                      width: 85,
                      height: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: warnatombol
                      ),
                      child: Row(
                        children: [
                          const Icon(
                            Icons.star,
                            size: 24,
                            color: Colors.yellow,
                          ),
                          const SizedBox(width: 3,),
                          Text(rating,
                          style: ConstTextStyle.stylepoppin(
                            fontWeight: FontWeight.w700,
                            fontsize: 13,                                            
                            ),
                          )
                        ],
                      ),
                    ),
                  )
              ],
            ),
          ),
          const SizedBox(height: 5,),
          Text(restoName,
          style: ConstTextStyle.stylepoppin(
            fontsize: 15,
            fontWeight: FontWeight.w600
            ),
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(height: 3,),
          Text(menutitle,
          style: ConstTextStyle.stylepoppin(
            fontsize: 13,
            fontWeight: FontWeight.w400
            ),            
          ),
          const SizedBox(height: 3,),
          Text(alamat,
          style: ConstTextStyle.stylepoppin(
            fontsize: 11,
            fontWeight: FontWeight.w300
            ),            
          ),
          const SizedBox(height: 3,),
          Text('Rp $harga.000',
          style: ConstTextStyle.stylepoppin(
            fontsize: 11,
            fontWeight: FontWeight.w300
            ),            
          ),
          const Spacer(),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(onPressed: () => favoriteController.addtoFavorite(index), 
            icon: Icon(
              isFavorit? Icons.favorite : Icons.favorite_outline,
              size: 24,
              color: Colors.red,
              ),
            ),
          )
        ],
      ),
    );
  }
}