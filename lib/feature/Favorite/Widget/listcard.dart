import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Favorite/Controller/Favorit_controller.dart';

class ListCard extends StatelessWidget {
  const ListCard({super.key, required this.imageUrl, required this.harga, required this.rating, required this.restoName, required this.alamat, required this.menutitle, required this.isFavorit, required this.index, required this.favoriteController});

  final String imageUrl;
  final String harga;
  final String rating;
  final String restoName;
  final String alamat;
  final String menutitle;
  final bool isFavorit;
  final int index;
  final FavoriteController favoriteController;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 190,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: warnakotak
      ),
      padding: const EdgeInsets.all(20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 130,
            child: Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    imageUrl,
                    width: 130,
                    height: 130,
                    fit: BoxFit.cover,
                  ),
                ),
                Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 11,
                          vertical: 3
                        ),
                        width: 85,
                        height: 30,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: warnatombol
                        ),
                        child: Row(
                          children: [
                            const Icon(
                              Icons.star,
                              size: 24,
                              color: Colors.yellow,
                            ),
                            const SizedBox(width: 3,),
                            Text(rating,
                            style: ConstTextStyle.stylepoppin(
                              fontWeight: FontWeight.w700,
                              fontsize: 13,                                            
                              ),
                            )
                          ],
                        ),
                      ),
                    )
              ],
            ),
          ),
          const SizedBox(width: 5,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(restoName,
                style: ConstTextStyle.stylepoppin(
                  fontsize: 16,
                  fontWeight: FontWeight.w600,
                  color: warnaputih
                  ),
                ),
                const SizedBox(width: 5,),
                Text(menutitle,
                style: ConstTextStyle.stylepoppin(
                  fontsize: 16,
                  fontWeight: FontWeight.w400,
                  color: warnaputih
                  ),
                ),
                const SizedBox(width: 5,),
                Text(alamat,
                style: ConstTextStyle.stylepoppin(
                  fontsize: 15,
                  fontWeight: FontWeight.w400,
                  color: warnaputih
                  ),
                ),
                const SizedBox(width: 5,),
                Text('Rp. $harga.000',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 14,
                  fontWeight: FontWeight.w300,
                  color: warnaputih
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(width: 10,),
          Align(
            alignment: Alignment.bottomRight,
            child: IconButton(onPressed: () => favoriteController.addtoFavorite(index), 
            icon: Icon(
              isFavorit? Icons.favorite : Icons.favorite_outline,
              size: 24,
              color: Colors.red,
              ),
            ),
          ),
        ],
      ),
    );
  }
}