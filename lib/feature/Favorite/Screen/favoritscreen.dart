import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Favorite/Controller/Favorit_controller.dart';
import 'package:vaionapp/feature/Favorite/Widget/grid_card.dart';
import 'package:vaionapp/feature/Favorite/Widget/listcard.dart';

class FavoritScreen extends StatelessWidget {
  FavoritScreen({super.key});

  final FavoriteController _favoriteController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text('Beli Lagi',
                      style: ConstTextStyle.stylepoppin(
                        fontsize: 20,
                        fontWeight: FontWeight.bold
                        ),
                      ),
                      const SizedBox(height: 5,),
                      Container(
                        width: 85,
                        height: 1,
                        color: warnatombol,
                      ),
                    ],
                  ),
                  Obx(() => IconButton(
                    onPressed: () => _favoriteController.tooggleIsGrid(), 
                    icon: Icon(
                      _favoriteController.isGrid.isFalse 
                      ? Icons.grid_view
                      : Icons.list,
                    size: 32,
                    color: warnaputih,
                    )
                  ),
                  )
                  
                ],
              ),
              const SizedBox(height: 20,),
              Obx(() {
                if(_favoriteController.isGrid.isTrue){
                  return GridView.builder(
                shrinkWrap: true,
                  itemCount: _favoriteController.favoriteList.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisExtent: 300,
                    crossAxisCount: 2,
                    mainAxisSpacing: 20,
                    ), 
                  itemBuilder: (context, index) {
                    final data = _favoriteController.favoriteList[index];
                    return GridCard(
                      imageUrl: data.imageUrl,
                      alamat: data.alamat,
                      harga: data.harga,
                      isFavorit: data.isFavorit,
                      menutitle: data.menutitle,
                      restoName: data.restoName,
                      rating: data.rating, 
                      index: index, 
                      favoriteController: _favoriteController,
                    );
                  },
                );
                }
                return ListView.builder(
                itemCount: _favoriteController.favoriteList.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                    final data = _favoriteController.favoriteList[index];
                    return Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: ListCard(
                        imageUrl: data.imageUrl,
                        alamat: data.alamat,
                        harga: data.harga,
                        isFavorit: data.isFavorit,
                        menutitle: data.menutitle,
                        restoName: data.restoName,
                        rating: data.rating, 
                        index: index, 
                        favoriteController: _favoriteController,
                      ),
                    );
                  },
              );
              }),              
            ],
          ),
        ),
      )
    );
  }
}