import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/onboard/controller/onboard_controller.dart';
import 'package:vaionapp/route/route.dart';

class OnboardScreen extends StatelessWidget {
  OnboardScreen({super.key});

  final onboardController = Get.put(OnboardController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView.builder(
            onPageChanged: (value) {
              onboardController.indeximage.value = value;
            },
            itemCount: onboardController.imagersOnboard.length,
            itemBuilder: (context, index) => Container(
              decoration: BoxDecoration(
                color: warnabackground,
                image: DecorationImage(
                  image: AssetImage(onboardController.imagersOnboard [index],
                  ), fit: BoxFit.cover
                )
              ),
            ),
          ),
          Positioned(
            bottom: 50,
            left: 61,
            right: 61,

            child: Column(
              children: [
                ElevatedButton(
                  style: ButtonStyle(
                    padding: const MaterialStatePropertyAll(EdgeInsets.symmetric(
                      vertical: 7,
                      horizontal: 10,
                      )
                    ),
                    backgroundColor: const MaterialStatePropertyAll(warnatombol),
                    shape: MaterialStatePropertyAll(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    minimumSize: const MaterialStatePropertyAll(
                      Size(237, 54),
                    ),
                  ),                  
                  
                  onPressed: () => Get.toNamed(Routename.signupScreen), 
                child: Text(
                  'Masuk',
                  style: ConstTextStyle.styleinter(
                    fontsize: 15,
                    fontWeight: FontWeight.bold
                  ),)

                ),
                const SizedBox(
                  height: 10,
                ),
                Wrap(
                  spacing: 5,
                  children: List.generate(
                    onboardController.imagersOnboard.length, 
                    (index) => Obx(() => Container(
                      width: 40,
                      height: 3,
                      color: onboardController.indeximage.value == index
                        ? warnatombol 
                        : warnaputih,
                    )), )
                )
              ],
            )
          )
        ],
      ),
    );
  }
}