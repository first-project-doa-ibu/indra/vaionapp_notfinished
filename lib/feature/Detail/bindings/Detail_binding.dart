import 'package:get/get.dart';
import 'package:vaionapp/feature/Detail/Controller/DetailController.dart';

class DetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(DetailController());
  }

}