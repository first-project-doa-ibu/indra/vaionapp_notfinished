import 'package:get/get.dart';
import 'package:vaionapp/feature/modele/rekomendasi_model.dart';

class DetailController extends GetxController {
  final RxInt _itemCount = 1.obs;
  RxInt get itemCount => _itemCount;

  RxInt totalHarga(ProductModel productModel){
    return (_itemCount * int.parse(productModel.harga)).toInt().obs;
  }

  void incrementitem() {
    _itemCount.value++;
  }

  void decrementitem() {
    if(_itemCount.value > 1){
      _itemCount.value--;
    } else {
      Get.showSnackbar(
      const GetSnackBar(
        title: "Sudah mencapai batas Minimal",
        message: "Item Minimal Setidaknya 1 Pesanan",
        duration: Duration(seconds: 3),
      )
    );
    }
  }
}