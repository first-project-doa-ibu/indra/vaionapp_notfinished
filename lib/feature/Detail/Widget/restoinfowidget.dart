import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';

class RestoInfoWidget extends StatelessWidget {
  const RestoInfoWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 36),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: warnakotak,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: warnaputih
                  ),
                ),
                child: Image.asset('assets/images/LogoApk.png',
                  width: 30,
                  height: 30,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(width: 10,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Nama Toko',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 15,
                      fontWeight: FontWeight.w500
                      ),
                    ),
                    const SizedBox(height: 5,),
                    Text('Alamat',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 15,
                      fontWeight: FontWeight.w300
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          const SizedBox(height: 10,),
          ElevatedButton(
            onPressed: (){},
            style: ButtonStyle(
              backgroundColor: const MaterialStatePropertyAll(warnatombol),
              shape: MaterialStatePropertyAll(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                )
              ),
            ),
            child: Text('Kunjungi',
              style: ConstTextStyle.stylepoppin(fontsize: 13,
              fontWeight: FontWeight.bold
              ),
            ), 
          ),
        ],
      ),
    );
  }
}