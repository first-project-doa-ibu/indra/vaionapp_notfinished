import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/modele/rekomendasi_model.dart';

class HeaderDetailWidget extends StatelessWidget {
  const HeaderDetailWidget({
    super.key,
    required this.productModel,
  });

  final ProductModel productModel;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 300,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10) 
            ),
            image: DecorationImage(
              image: AssetImage(productModel.imageUrl),
              fit: BoxFit.cover  
            )
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 13, 
              horizontal: 35),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(productModel.label,
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15, 
                    fontWeight: FontWeight.w500
                    ),
                  ),
                  const SizedBox(height: 5,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(productModel.tag, style: ConstTextStyle.stylepoppin(
                        fontsize: 14, 
                        fontWeight: FontWeight.w300),
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.star, 
                            size: 24, 
                            color: Colors.yellow,),
                            const SizedBox(width: 5,),
                            Text(productModel.rating, 
                            style: ConstTextStyle.stylepoppin(
                              fontsize: 15,
                              fontWeight: FontWeight.w600
                            ),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
          top: 30,
          left: 16,
          child: InkWell(
            onTap: () => Get.back(),
            child: const Icon(
              Icons.arrow_back,
              size: 36,
              color: warnaputih,
            ),
          ),
        )
      ],
    );
  }
}