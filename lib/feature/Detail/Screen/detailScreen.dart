import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Detail/Controller/DetailController.dart';
import 'package:vaionapp/feature/Detail/Widget/Headerdetailwidget.dart';
import 'package:vaionapp/feature/Detail/Widget/restoinfowidget.dart';
import 'package:vaionapp/feature/modele/rekomendasi_model.dart';
import 'package:vaionapp/route/route.dart';

class DetailScreen extends StatelessWidget {
  DetailScreen({super.key, required this.productModel});

  final ProductModel productModel;

  final DetailController detailController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SingleChildScrollView(
        child: Column(
          children: [
            HeaderDetailWidget(productModel: productModel),
            const SizedBox(height: 20,),
            const RestoInfoWidget(),
            const SizedBox(height: 20,),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 36),
              padding: const EdgeInsets.symmetric(
                horizontal: 10, 
                vertical: 17),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: warnakotak
              ),
              child: Row(
                children: [
                  Container(
                    width: 110,
                    height: 110,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage(
                          productModel.imageUrl,
                          ),
                          fit: BoxFit.cover
                      )
                    ),
                  ),
                  const SizedBox(width: 10,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start ,
                      children: [
                        Text(productModel.label,
                          style: ConstTextStyle.stylepoppin(
                            fontsize: 15,
                            fontWeight: FontWeight.w700
                          ),
                        ),
                        const SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Obx(() => Container(
                              height: 34,
                              width: 34,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: warnatombol,
                                )
                              ),
                              child: Center(
                                child: Text('${detailController.itemCount}',
                                  style: ConstTextStyle.stylepoppin(
                                    fontsize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                            ),
                            Row(
                              children: [
                                InkWell(
                                  onTap: () => detailController.incrementitem(),
                                  child: Container(
                                    width: 34,
                                    height: 34,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: warnatombol
                                    ),
                                    child: const Center(
                                      child: Icon(
                                        Icons.add,
                                        size: 24,
                                        color: warnaputih,
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 10,),
                                InkWell(
                                  onTap: () => detailController.decrementitem(),
                                  child: Container(
                                    width: 34,
                                    height: 34,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: warnatombol
                                    ),
                                    child: const Center(
                                      child: Icon(
                                        Icons.remove,
                                        size: 24,
                                        color: warnaputih,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                        const SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Total', 
                              style: ConstTextStyle.stylepoppin(
                                fontsize: 15,
                                fontWeight: FontWeight.w300
                              ),
                            ),
                            Obx(() => Text('Rp${detailController.totalHarga(productModel).value}.000', 
                              style: ConstTextStyle.stylepoppin(
                                fontsize: 15,
                                fontWeight: FontWeight.w300
                              ),
                            )
                           
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 36),
              child: Align(
                alignment: Alignment.topRight,
                child: ElevatedButton(
                  onPressed: () => Get.toNamed(
                    Routename.orderScreen, 
                    arguments: {
                      'data': productModel.tojson(),
                      'totalharga': detailController.totalHarga(productModel).value,
                      'totalitem': detailController.itemCount.value,
                    }), 
                  style: ButtonStyle(
                    backgroundColor: const MaterialStatePropertyAll(warnatombol),
                    padding: const MaterialStatePropertyAll(
                      EdgeInsets.symmetric(horizontal: 65, vertical: 12),
                    ),
                    shape: MaterialStatePropertyAll(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      )
                    )
                  ),
                  child: Text('Pesan',
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.w600,
                    ),
                  )
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}



