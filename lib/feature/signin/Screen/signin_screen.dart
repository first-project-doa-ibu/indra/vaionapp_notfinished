import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/route/route.dart';
import 'package:vaionapp/widget/textfield.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SafeArea(
        bottom: false,
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          children: [
            const SizedBox(
              height: 20,
            ),
            Image.asset(
              'assets/images/LogoApk.png',
              height: 150,
            ),

            Container(
              height: 320,
              width: 160,
              decoration: BoxDecoration(
                color: warnakotak,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Stack(
                children: [
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Text(
                            'laman',
                            style: ConstTextStyle.styleinter(
                              color: warnaputih,
                              fontsize: 10,
                            ),
                          ),
                          Text(
                            'LOGIN',
                            style: ConstTextStyle.styleinter(
                              color: warnaputih,
                              fontsize: 30,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),

                          Textformfieldwidget(hintText: "username",),

                          const SizedBox( height: 10,),

                          Textformfieldwidget(
                            hintText: "Password",
                            Password: true,
                          ),

                          const SizedBox(height: 10,),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 0),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 2 * 24,
                            child: ElevatedButton(
                              onPressed: () => Get.toNamed(Routename.mainScreen),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: warnatombol, 
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)
                                ),
                              ), 
                              child: Text('Login',
                                style: ConstTextStyle.styleinter(
                                  fontsize: 20,
                                  fontWeight: FontWeight.w900,
                                  color: warnaputih),
                              ),
                            ),
                          ),

                          const SizedBox(height: 10,),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Belum punya akun ?',
                                style: ConstTextStyle.styleinter(
                                  fontsize: 10,
                                  color: warnaputih
                                ),
                              ),

                              const SizedBox(width: 5,),

                              InkWell(
                                onTap: () => Get.offAllNamed(Routename.signupScreen),
                                child: Text('Register',
                                  style: ConstTextStyle.styleinter(
                                    fontsize: 10,
                                    color: warnatombol,
                                  ),
                                ),
                              ),
                            ],
                          )

                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
           const SizedBox(
              height: 15,
            ),

            const SizedBox(height: 50,)

          ],
        ),
      ),
    );
  }
}
