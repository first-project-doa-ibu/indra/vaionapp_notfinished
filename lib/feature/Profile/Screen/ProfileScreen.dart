import 'package:flutter/material.dart';
import 'package:glass_kit/glass_kit.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/Header_widget.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              const Headerwidget(),
              const SizedBox(height: 20,),
              Stack(
                children: [
                  GlassContainer.clearGlass(
                    height: 300,
                    width: 400,
                    gradient: LinearGradient(
                      colors: [Colors.white.withOpacity(0.40), Colors.white.withOpacity(0.10)],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    
                    blur: 15.0,
                    shadowColor: Colors.black.withOpacity(0.20),
                    padding: const EdgeInsets.all(8.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 135),
                    child: Center(
                      child: Column(
                        children: [
                          Text('Under Maintenance',
                            style: ConstTextStyle.stylepoppin(
                              fontsize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      )
    );
  }
}