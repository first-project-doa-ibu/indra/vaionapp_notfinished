import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Promo/controller/promo_controller.dart';
import 'package:vaionapp/feature/Promo/widget/kartuuntukmu.dart';

class PromoScreen extends StatelessWidget {
  PromoScreen({super.key});

  final PromoController promoController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 20,),
                    Text('Ada Banyak Promo\nUntuk Kamu',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 20, 
                      fontWeight: FontWeight.bold,
                      color: warnaputih,
                      height: 1.3,
                      ),
                    ),
                    const SizedBox(height: 5,),
                    Container(
                        width: 130,
                        height: 1,
                        color: warnatombol,
                    ),
                    const SizedBox(height: 10,),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              Container(
                                padding: const EdgeInsetsDirectional.all(13),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                    color: const Color.fromARGB(255, 101, 101, 101),
                                    width: 2,
                                  )
                                ),
                                child: Center(
                                  child: Image.asset('assets/images/food.png',
                                  fit: BoxFit.cover,
                                  width: 75,
                                  height: 75,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 5,),
                              Text('food',
                                style: ConstTextStyle.stylepoppin(
                                  fontWeight: FontWeight.bold,
                                  fontsize: 15,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(width: 10,),
                          Column(
                            children: [
                              Container(
                                padding: const EdgeInsetsDirectional.all(13),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                    color: const Color.fromARGB(255, 101, 101, 101),
                                    width: 2,
                                  )
                                ),
                                child: Center(
                                  child: Image.asset('assets/images/Icecream.png',
                                  fit: BoxFit.cover,
                                  width: 75,
                                  height: 75,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 5,),
                              Text('Ice Cream',
                                style: ConstTextStyle.stylepoppin(
                                  fontWeight: FontWeight.bold,
                                  fontsize: 15,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    
                  ],
                ),
              ),
              const SizedBox(height: 20,),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Diskon jumat',
                              style: ConstTextStyle.stylepoppin(
                                fontsize: 15,
                                fontWeight: FontWeight.bold
                                ),
                              ),
                              Row(                                
                                children: [
                                  Text('More',
                                  style: ConstTextStyle.stylepoppin(
                                    fontsize: 15,
                                    fontWeight: FontWeight.w400,
                                    color: warnatombol
                                    ),
                                  ),
                                  const SizedBox(width: 5,),
                                  const Icon(
                                    Icons.chevron_right,
                                    size: 24,
                                    color: warnatombol,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: promoController.kartuuntukmuList.length,
                            itemBuilder: (context, index) {
                              final data = promoController.kartuuntukmuList[index];
                              return Container(
                              margin: const EdgeInsets.symmetric(horizontal: 5),
                              child: KartuUntukmu(
                                restoname: data.restoName, 
                                menuTitle: data.menutitle,
                                rating: data.rating,
                                imageUrl: data.imageUrl,
                                alamat: data.alamat,
                                discount: data.discount,
                                isDiscount: data.isDiscount,
                                )
                              );
                            }
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Hanya sekarang',
                              style: ConstTextStyle.stylepoppin(
                                fontsize: 15,
                                fontWeight: FontWeight.bold
                                ),
                              ),
                              Row(                                
                                children: [
                                  Text('More',
                                  style: ConstTextStyle.stylepoppin(
                                    fontsize: 15,
                                    fontWeight: FontWeight.w400,
                                    color: warnatombol
                                    ),
                                  ),
                                  const SizedBox(width: 5,),
                                  const Icon(
                                    Icons.chevron_right,
                                    size: 24,
                                    color: warnatombol,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: promoController.diskonjumatList.length,
                            itemBuilder: (context, index) {
                              final data = promoController.diskonjumatList[index];
                              return Container(
                              margin: const EdgeInsets.symmetric(horizontal: 5),
                              child: KartuUntukmu(
                                restoname: data.restoName, 
                                menuTitle: data.menutitle,
                                rating: data.rating,
                                imageUrl: data.imageUrl,
                                alamat: data.alamat,
                                discount: data.discount,
                                isDiscount: data.isDiscount,
                                )
                              );
                            }
                          ),
                        ),
                      ],
                    )
            ],
          ),
        ))
    );
  }
}

