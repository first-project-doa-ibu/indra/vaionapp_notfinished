import 'package:get/get.dart';
import 'package:vaionapp/feature/modele/kartuuntukmumodel.dart';

class PromoController extends GetxController {
  final List<KartuuntukmuModel> kartuuntukmuList = [
    KartuuntukmuModel(
      restoName: 'Warung haji', 
      menutitle: 'Burger Tikus', 
      alamat: 'Jl. Sempoyongan', 
      isDiscount: true, 
      discount: '40',
      imageUrl: 'assets/images/burger.png',
      rating: '4.5'
      
      ),
      KartuuntukmuModel(
      restoName: 'Warung hafiz', 
      menutitle: 'Basic Icecream', 
      alamat: 'Jl. Sempojangan', 
      isDiscount: false, 
      discount: '0',
      imageUrl: 'assets/images/eskrim1.jpg',
      rating: '4.2'
      ),
  ];

  final List<KartuuntukmuModel> diskonjumatList = [
    KartuuntukmuModel(
      restoName: 'Warung haji', 
      menutitle: 'Pizza pineapple', 
      alamat: 'Jl. Sempoyongan', 
      isDiscount: true, 
      discount: '40',
      imageUrl: 'assets/images/pizza.png',
      rating: '4.7'
      
      ),
      KartuuntukmuModel(
      restoName: 'Warung jainudin', 
      menutitle: 'Ayam Presto', 
      alamat: 'Jl. jatidiri', 
      isDiscount: true, 
      discount: '35',
      imageUrl: 'assets/images/ayampresto.jpg',
      rating: '4.4'
      ),
  ];
}