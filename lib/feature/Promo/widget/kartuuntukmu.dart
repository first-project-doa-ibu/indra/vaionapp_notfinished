import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';

class KartuUntukmu extends StatelessWidget {
  const KartuUntukmu({
    super.key, 
    required this.restoname, 
    required this.menuTitle, 
    required this.alamat, 
    required this.isDiscount, 
    required this.discount,
    required this.imageUrl,
    required this.rating,
  });

  final String restoname;
  final String menuTitle;
  final String alamat;
  final bool isDiscount;
  final String discount;
  final String imageUrl;
  final String rating;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 170,
      decoration: BoxDecoration(
        color: warnakotak,
        borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 170,
            height: 100,
            child: Stack(
              children: [
                Container(
                  width: 170,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage(imageUrl),
                      fit: BoxFit.cover
                    )
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 11,
                      vertical: 3
                    ),
                    width: 85,
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: warnatombol
                    ),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.star,
                          size: 24,
                          color: Colors.yellow,
                        ),
                        const SizedBox(width: 3,),
                        Text(rating,
                        style: ConstTextStyle.stylepoppin(
                          fontWeight: FontWeight.w700,
                          fontsize: 13,                                            
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 5,),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 5
            ).copyWith(bottom: 13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(restoname,
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.bold
                    ),                              
                  ),
                  const SizedBox(height: 5,),
                  Text(menuTitle,
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.w500
                    ),                              
                  ),
                  const SizedBox(height: 5,),
                  Text(alamat,
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.w300
                    ),                              
                  ),
                  const SizedBox( height: 5,),
                  if(isDiscount==true || int.parse(discount) >0) 
                  InkWell(
                    child: Container(
                      width: 100,
                      height: 20,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 5,                                          
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: warnatombol
                      ),
                      child: Center(
                        child: Text('$discount%',
                        style: ConstTextStyle.stylepoppin(
                          fontsize: 10,
                          fontWeight: FontWeight.w600
                        ),
                        ),
                      ),
                    ),
                  )
              ],
            ),
          )
        ],                      
      ),
    );
  }
}