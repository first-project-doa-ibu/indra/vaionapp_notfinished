import 'dart:developer';
import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/firebase_collection.dart';

class AuthController extends GetxController{

  final TextEditingController userNameController = TextEditingController();
  final TextEditingController passwirdController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  // CollectionReference userCollection = 
  // FirebaseCollectionConstants.getUserCollection();

  final RxBool _isObsecure = false.obs;
  RxBool get isObsecure => _isObsecure;

  // Future<void> signUpUsers() async {
  //   try {
  //     userCollection.add();
  //   } catch(e, st) {
  //     log('Error From Sign Up $e, stack trace $st');
  //     Get.showSnackbar(GetSnackBar(
  //       title: 'Somthing Wrong',
  //       message: 'i can feel it $e',
  //     ));
  //   }
  // }

  void changeIsObsecure() {
    _isObsecure.value = !_isObsecure.value;
  }
}