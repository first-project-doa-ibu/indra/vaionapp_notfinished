import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/modele/rekomendasi_model.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({super.key, required this.productModel, required this.totalharga, required this.totalitem});

  final ProductModel productModel;
  final int totalharga;
  final int totalitem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      appBar: AppBar(
        backgroundColor: warnabackground,
        elevation: 0,
        title: Text('Pesanan',
          style: ConstTextStyle.stylepoppin(
            fontsize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
        leading: IconButton(
          onPressed: () => Get.back(), 
          icon: const Icon(Icons.arrow_back_ios_new, size: 25, color: warnaputih,),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset('assets/images/Lokasi.png',
                width: 40,
                height: 40,
                ),
                const SizedBox(width: 5,),
                Text("Lokasi Anda",
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
            const SizedBox(height: 10,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Home',
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                  ),
                ),
                Text('Alamat',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 15,
                  fontWeight: FontWeight.normal
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10,),
            Row(
              children: [
                ElevatedButton(
                  onPressed: (){}, 
                  style: ButtonStyle(
                  backgroundColor: const MaterialStatePropertyAll(warnatombol),
                  padding: const MaterialStatePropertyAll(
                    EdgeInsets.symmetric(horizontal: 23,vertical: 13)
                  ),
                  shape: MaterialStatePropertyAll(
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                    )
                  ),
                  child: Text('Ubah Alamat',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 15,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 15, 
                vertical: 11
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: warnakotak
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text('Delevery',
                          style: ConstTextStyle.stylepoppin(
                            fontsize: 15,
                            fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                      const SizedBox(height: 10,),
                      Row(
                        children: [
                          Text('Waktu tiba',
                          style: ConstTextStyle.stylepoppin(
                            fontsize: 15,
                            fontWeight: FontWeight.normal
                          ),)
                        ],
                      ),
                    ],
                  ),
                  ElevatedButton(
                  onPressed: (){}, 
                  style: ButtonStyle(
                  backgroundColor: const MaterialStatePropertyAll(warnatombol),
                  padding: const MaterialStatePropertyAll(
                    EdgeInsets.symmetric(horizontal: 23,vertical: 13)
                  ),
                  shape: MaterialStatePropertyAll(
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                    )
                  ),
                  child: Text('Ubah',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 15,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Orderan Anda',
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.bold
                  ),
                ),
                const SizedBox(height: 10,),
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15, 
                    vertical: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: warnakotak,
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 70,
                        height: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                            image: AssetImage(productModel.imageUrl),
                            fit: BoxFit.cover  
                          )
                        ),
                      ),
                      const SizedBox(width: 5,),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(productModel.label,
                              style: ConstTextStyle.stylepoppin(
                                fontWeight: FontWeight.w700,
                                fontsize: 15,
                              ),
                            ),
                            const SizedBox(height: 5,),
                            Text('Rp $totalharga.000',
                              style: ConstTextStyle.stylepoppin(
                                fontWeight: FontWeight.w300,
                                fontsize: 15,
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      hintText: 'NOOO',
                                      hintStyle: ConstTextStyle.stylepoppin(
                                        color: warnaputih,
                                        fontsize: 10,
                                        fontWeight: FontWeight.normal
                                      ),
                                      fillColor: Colors.grey,
                                      filled: true,
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                          color: warnatombol
                                        )
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                          color: warnatombol
                                        )
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 20,),
                                ElevatedButton(
                                  onPressed: (){}, 
                                  style: ButtonStyle(
                                  backgroundColor: const MaterialStatePropertyAll(warnatombol),
                                  padding: const MaterialStatePropertyAll(
                                    EdgeInsets.symmetric(horizontal: 10,vertical: 6)
                                  ),
                                  shape: MaterialStatePropertyAll(
                                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                                    )
                                  ),
                                  child: Text('Ubah',
                                    style: ConstTextStyle.stylepoppin(
                                      fontsize: 15,
                                      fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                ElevatedButton(
                  onPressed: (){}, 
                  style: ButtonStyle(
                    minimumSize: MaterialStatePropertyAll(Size(
                      MediaQuery.of(context).size.width, 60)),
                  backgroundColor: const MaterialStatePropertyAll(warnatombol),
                  padding: const MaterialStatePropertyAll(
                    EdgeInsets.symmetric(horizontal: 23,vertical: 13)
                  ),
                  shape: MaterialStatePropertyAll(
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                    )
                  ),
                  child: Text('Order Sekarang',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 20,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      )
    );
  }
}