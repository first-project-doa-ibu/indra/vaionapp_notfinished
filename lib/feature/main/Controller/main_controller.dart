import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:vaionapp/ConstEL/color.dart';
// import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Favorite/Screen/favoritscreen.dart';
import 'package:vaionapp/feature/Home/Screen/homescreen.dart';
import 'package:vaionapp/feature/Profile/Screen/ProfileScreen.dart';
import 'package:vaionapp/feature/Promo/Screen/promo_screen.dart';


class MainController extends GetxController {
  final RxInt _indexBottonNavbar = 0.obs;
  RxInt get indexBottonNavbar => _indexBottonNavbar;

  void changeIndex(int val) {
    _indexBottonNavbar.value = val;
  }
  
  final List<BottomNavigationBarItem> listbuttonnavbar = [
          BottomNavigationBarItem(
            label: '',
            activeIcon: Container(
              height: 30, 
              margin: const EdgeInsets.only(top: 10),
              child: Container(
                width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/home_select.png'),
                ),
              ),
              ),
            ),
            icon: Container(
              margin: const EdgeInsets.only(top: 10),
              width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/home.png'),
                ),
              ),
            ),
          ),
          BottomNavigationBarItem(
            label: '',
            activeIcon: Container(
              height: 30, 
              margin: const EdgeInsets.only(top: 10),
              child: Container(
                width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/promo_select.png'),
                ),
              ),
              ),
            ),
            icon: Container(
              margin: const EdgeInsets.only(top: 10),
              width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/promo.png'),
                ),
              ),
            ),
          ),
          BottomNavigationBarItem(
            label: '',
            activeIcon: Container(
              height: 30, 
              margin: const EdgeInsets.only(top: 10),
              child: Container(
                width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/favorite_select.png'),
                ),
              ),
              ),
            ),
            icon: Container(
              margin: const EdgeInsets.only(top: 10),
              width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/favorite.png'),
                ),
              ),
            ),
          ),
          BottomNavigationBarItem(
            label: '',
            activeIcon: Container(
              height: 30, 
              margin: const EdgeInsets.only(top: 10),
              child: Container(
                width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/notif_select.png'),
                ),
              ),
              ),
            ),
            icon: Container(
              margin: const EdgeInsets.only(top: 10),
              width: 30,
              height: 30,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/notif.png'),
                ),
              ),
            ),
          ),
        ];

  final List<Widget> BodyMain = [
    HomeScreen(),
    PromoScreen(),
    FavoritScreen(),
    const ProfileScreen()
  ];

  
}