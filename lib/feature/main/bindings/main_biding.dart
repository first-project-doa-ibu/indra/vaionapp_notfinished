import 'package:get/get.dart';
import 'package:vaionapp/feature/Favorite/Controller/Favorit_controller.dart';
import 'package:vaionapp/feature/Promo/controller/promo_controller.dart';
import 'package:vaionapp/feature/main/Controller/main_controller.dart';

class MainBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(PromoController());
    Get.put(FavoriteController());
    Get.put(MainController());
    print('MAIN BINDING CALLED');
  }
  
}