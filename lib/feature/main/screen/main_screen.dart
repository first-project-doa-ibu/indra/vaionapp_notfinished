import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/feature/main/Controller/main_controller.dart';

class MainScreen extends StatelessWidget {
  MainScreen({super.key});

  final MainController mainc = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      bottomNavigationBar: Obx(()=> BottomNavigationBar(
        currentIndex: mainc.indexBottonNavbar.value,
        onTap: (value) => mainc.changeIndex(value),
        type: BottomNavigationBarType.fixed,
        backgroundColor: warnakotak,
        items: mainc.listbuttonnavbar,
        ),
      ),
      body: Obx(() => mainc.BodyMain[mainc.indexBottonNavbar.value]),
    );
  }
}