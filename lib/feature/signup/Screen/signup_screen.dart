import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/route/route.dart';
import 'package:vaionapp/widget/textfield.dart';

class SignupScreen extends StatelessWidget {
  const SignupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SafeArea(
        bottom: false,
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          children: [
            const SizedBox(
              height: 20,
            ),
            Image.asset(
              'assets/images/LogoApk.png',
              height: 150,
            ),

            Container(
              height: 380,
              width: 160,
              decoration: BoxDecoration(
                color: warnakotak,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Stack(
                children: [
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Text(
                            'laman',
                            style: ConstTextStyle.styleinter(
                              color: warnaputih,
                              fontsize: 10,
                            ),
                          ),
                          Text(
                            'REGISTER',
                            style: ConstTextStyle.styleinter(
                              color: warnaputih,
                              fontsize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          
                          Textformfieldwidget(hintText: "username",),

                          const SizedBox(height: 10,),

                          Textformfieldwidget(hintText: "email"),

                          const SizedBox(height: 10,),

                          Textformfieldwidget(
                            hintText: "Password",
                            Password: true,
                            ),

                          const SizedBox(height: 10,),
                          
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 0),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 2 * 24,
                            child: ElevatedButton(
                              onPressed: () => Get.offAllNamed(Routename.signinScreen),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: warnatombol, 
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15)
                                ),
                              ), 
                              child: Text('REGISTER',
                                style: ConstTextStyle.styleinter(
                                  fontsize: 20,
                                  fontWeight: FontWeight.w900,
                                  color: warnaputih),
                              ),
                            ),
                          ),
                          const SizedBox(height: 10,),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Sudah punya akun ?',
                                style: ConstTextStyle.styleinter(
                                  fontsize: 10,
                                  color: warnaputih
                                ),
                              ),

                              const SizedBox(width: 5,),

                              InkWell(
                                onTap: () => Get.offAllNamed(Routename.signinScreen),
                                child: Text('Login',
                                  style: ConstTextStyle.styleinter(
                                    fontsize: 10,
                                    color: warnatombol,
                                  ),
                                ),
                              ),
                            ],
                          )

                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}