import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/modele/rekomendasi_model.dart';

class HomeController extends GetxController {
  PageController? pageController;
  
  final RxInt _indexSlider = 0.obs;
  RxInt get indexSlider => _indexSlider;

  void changeindex (int value){
    _indexSlider.value = value;
  }

  final List<Map<String, dynamic>> cardList = [
    {
      'imageUrl':'assets/images/eskrim.png',
      'isCenter':false,
      'contentWidget': Column(
              children: [
                Text('Dapatkan',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w200
                  ),
                ),
                const SizedBox(height: 5,),
                Text('Promo eskrim',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w500
                  ),
                ),
                const SizedBox(height: 5,),
                Text('Dengan Promo',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w300
                  ),
                ),
                const SizedBox(height: 5,),
                Text('80%',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 30,
                  fontWeight: FontWeight.w900
                  ),
                ),
              ],
            ),
    },
    {
      'imageUrl':'assets/images/sup.png',
      'isCenter':true,
      'contentWidget': Column(
              children: [
                Text('Dapatkan 1',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w200
                  ),
                ),
                const SizedBox(height: 5,),
                Text('Promo eskrim',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w500
                  ),
                ),
                const SizedBox(height: 5,),
                Text('Dengan Sup',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w300
                  ),
                ),
                const SizedBox(height: 5,),
                Text('75%',
                style: ConstTextStyle.stylepoppin(
                  fontsize: 30,
                  fontWeight: FontWeight.w900
                  ),
                ),
              ],
            ),
      },
  ];

  final categoryList = [
    {
      'imageUrl':'assets/images/food.png',
      'label':'Food'
    },
    {
      'imageUrl':'assets/images/drink.png',
      'label':'Drink'
    },
    {
      'imageUrl':'assets/images/Icecream.png',
      'label':'Ice cream'
    },
  ];

  final List<ProductModel> favoriteList = [
    ProductModel(
      imageUrl: 'assets/images/burger.png', 
      label: 'Burger Tikus', 
      rating: '4.5', 
      harga: '28',
      tag: ''
    ),
    ProductModel(
      imageUrl: 'assets/images/kepal.png', 
      label: 'Strawberry & Choco', 
      rating: '4.1', 
      harga: '21', 
      tag: ''
    ),
    ProductModel(
      imageUrl: 'assets/images/pizza.png', 
      label: 'Pizza Pineapple', 
      rating: '4.7', 
      harga: '52', 
      tag: ''
    ),
  ];

  // final favoriteList = [
  //   {
  //     'imageUrl':'assets/images/burger.png',
  //     'label':'Burger Tikus',
  //     'rating':'4.5',
  //     'harga':'Rp. 28.000',
  //   },
  //   {
  //     'imageUrl':'assets/images/kepal.png',
  //     'label':'Strawberry & Choco',
  //     'rating':'4.1',
  //     'harga':'Rp. 21.000',
  //   },
  //   {
  //     'imageUrl':'assets/images/pizza.png',
  //     'label':'Pizza Pineapple',
  //     'rating':'4.7',
  //     'harga':'Rp. 52.000',
  //   },
  // ];

  final List<ProductModel> rekomendasiList = [
    ProductModel(
      imageUrl: 'assets/images/eskrim1.jpg', 
      label: 'Basic Ice Cream', 
      rating: '4.2', 
      harga: '15', 
      tag: 'Choco, Strawberry, Vanilla'),
      ProductModel(
      imageUrl: 'assets/images/ayampresto.jpg', 
      label: 'Ayam Presto', 
      rating: '4.4', 
      harga: '45', 
      tag: 'Idk'
      ),
  ];

  @override
  void onInit() {
    super.onInit();
    pageController = PageController(viewportFraction: 0.85);
  }
}