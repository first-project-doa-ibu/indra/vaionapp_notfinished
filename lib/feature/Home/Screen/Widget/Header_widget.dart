import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/font.dart';

class Headerwidget extends StatelessWidget {
  const Headerwidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 36),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Hi',
              style: ConstTextStyle.stylepoppin(
                fontsize: 16,
                fontWeight: FontWeight.w400
                ),
              ),
              const SizedBox(height: 5,),
              Text('Dominik Szoboslai',
              style: ConstTextStyle.stylepoppin(
                fontsize: 16,
                fontWeight: FontWeight.w900
                ),
              ),
              const SizedBox(height: 5,),
              Row(
                children: [
                  const Icon(Icons.location_pin, color: Colors.red, size: 22,),
                  const SizedBox(width: 5,),
                  Text('Jakarta',
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 15,
                    fontWeight: FontWeight.w500
                  ),
                  )
                ],
              )
            ],
          ),
          ClipOval(
            child: Container(
              width: 80,
              height: 80,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage('https://i.pinimg.com/564x/37/87/d6/3787d6cb34e4ebc9604db941a3cc0d64.jpg', 
                  ), 
                  fit: BoxFit.cover,)
              ),
            ),
          )
        ],
      ),
    );
  }
}