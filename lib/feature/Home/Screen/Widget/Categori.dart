import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Home/controller/home_controller.dart';

class CategoryWidget extends StatelessWidget {
  const CategoryWidget({super.key, required this.homeController});

  final HomeController homeController;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 65),
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(height: 69,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: homeController.categoryList.length,
                  itemBuilder: (content, index) => Container(
                    margin: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      children: [
                        Expanded(
                          child: Image.asset(
                            homeController.categoryList[index]['imageUrl']!),
                        ),
                        const SizedBox(height: 5,),
                        Text(homeController.categoryList[index]['label']!, 
                          style: ConstTextStyle.stylepoppin(
                            fontWeight: FontWeight.w100,
                            fontsize: 10,
                            color: warnaputih
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
    );
  }
}