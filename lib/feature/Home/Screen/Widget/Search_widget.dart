import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';

class Searchwidget extends StatelessWidget {
  const Searchwidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal : 36),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: "search",
          hintStyle: ConstTextStyle.stylepoppin(
            fontsize: 15, 
            fontWeight: FontWeight.w300,
            color: Colors.grey
          ),
          suffixIcon: const Icon(
            Icons.search, 
            size: 24, 
            color: Colors.grey,
          ),
          filled: true,
          fillColor: warnainput,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: const BorderSide(
              color: warnainput
            )
          )
        ),
      ),
    );
  }
}