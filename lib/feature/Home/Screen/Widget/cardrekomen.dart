import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';

class CardRekomendasi extends StatelessWidget {
  const CardRekomendasi({
    super.key, required this.imageUrl, required this.label, required this.rating, required this.harga, required this.tag,
  });

  final String imageUrl;
  final String label;
  final String rating;
  final String harga;
  final String tag;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 168,
      width: 320,
      padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 10),
      decoration: BoxDecoration(
        color: warnakotak,
        borderRadius: BorderRadius.circular(20)
      ),
      child: Row(
        children: [
          Container(
            width: 115,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              border: Border.all(color: warnaputih, width: 2),
              image: DecorationImage(
                image: AssetImage(imageUrl),
                fit: BoxFit.cover
              ),
            ),
          ),
          const SizedBox(width: 10,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(label,
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.bold
                  ),
                ),
                const SizedBox(height: 5,),
                Text(tag,
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.w400
                  ),
                ),
                const SizedBox(height: 5,),
                Row(
                  children: [
                    const Icon(Icons.star, color: Colors.yellow,),
                    Text(rating, 
                      style: ConstTextStyle.stylepoppin(
                      fontsize: 10,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                  ],
                ),
                const SizedBox(height: 5,),
                Text('Rp $harga.000', 
                style: ConstTextStyle.stylepoppin(
                  fontsize: 10,
                  fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: 83,
              height: 22,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: warnatombol
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text('Pesan', 
                  style: ConstTextStyle.stylepoppin(
                    fontsize: 10,
                    fontWeight: FontWeight.bold
                    ),
                  ),
              ),                                  
            ),
          ),
        ],
      ),
    );
  }
}
