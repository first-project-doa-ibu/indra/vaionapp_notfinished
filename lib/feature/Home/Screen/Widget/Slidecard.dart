import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/cardslider.dart';
import 'package:vaionapp/feature/Home/controller/home_controller.dart';

class Slidercard extends StatelessWidget {
  const Slidercard({super.key, required this.homeController});

  final HomeController homeController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 130,
            child: PageView.builder(
              onPageChanged: (value) => homeController.changeindex(value),
              controller: homeController.pageController,
              itemCount: homeController.cardList.length,
              itemBuilder: (context, index) => Container(
                margin:const EdgeInsets.symmetric(horizontal: 10),
                child: Cardslide(
                  imageUrl: homeController.cardList[index]['imageUrl'],
                  contentWidget: homeController.cardList[index]['contentWidget'],
                  isCenter: homeController.cardList[index]["isCenter"],
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20,),
            Container(
              margin: const EdgeInsets.only(left: 40),
              alignment: Alignment.topLeft,
              child: Wrap(
                    spacing: 5,
                    children: List.generate(
                      homeController.cardList.length, 
                      (index) => Obx(() => Container(
                        width: 40,
                        height: 3,
                        color: homeController.indexSlider.value == index?warnatombol :warnaputih,
                      ),
                    ),
                  ),
                ),
            )
      ],
    );
  }
}