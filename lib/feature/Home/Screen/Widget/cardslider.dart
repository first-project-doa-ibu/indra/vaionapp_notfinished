import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';

class Cardslide extends StatelessWidget {
  const Cardslide({
    super.key, required this.imageUrl, required this.contentWidget, this.isCenter=false,
  });

  final String imageUrl;
  final Widget contentWidget;
  final bool? isCenter;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 280,
      height: 130,
      // color: warnakotak,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 100,
              decoration: BoxDecoration(
                color: warnatombol,
                borderRadius: BorderRadius.circular(24)
              ),
            ),
          ),
          Positioned(
            left: 30,
            top: isCenter==true ?50 :null,
            child: Image.asset(
              imageUrl,
              width: 60,
            )
          ),
          Positioned(
            top: 40,
            left: 150,
            child: contentWidget,
          ),
        ],
      ),
    );
  }
}


