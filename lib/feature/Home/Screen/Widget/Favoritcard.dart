import 'package:flutter/material.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';

class FavoriteCard extends StatelessWidget {
  const FavoriteCard({super.key, required this.label, required this.imageUrl, required this.harga, required this.rating});

  final String label;
  final String imageUrl;
  final String harga;
  final String rating;

  @override
  Widget build(BuildContext context) {
    return Container(
                    width: 206,
                    height: 168,
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: warnakotak
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          imageUrl),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(height: 10,),
                                  Text(label,
                                  style: ConstTextStyle.stylepoppin(
                                    fontsize: 10,
                                    fontWeight: FontWeight.bold
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          const Icon(Icons.star, color: Colors.yellow,),
                                          Text(rating, 
                                            style: ConstTextStyle.stylepoppin(
                                            fontsize: 10,
                                            fontWeight: FontWeight.bold
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text('Rp $harga.000', style: ConstTextStyle.stylepoppin(
                                    fontsize: 10,
                                    fontWeight: FontWeight.bold
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            )
                      ],
                    ),
                  );
  }
}