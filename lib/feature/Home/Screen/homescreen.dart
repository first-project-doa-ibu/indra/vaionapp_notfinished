import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/Categori.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/Favoritcard.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/Header_widget.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/Search_widget.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/Slidecard.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/cardrekomen.dart';
import 'package:vaionapp/feature/Home/Screen/Widget/cardslider.dart';
import 'package:vaionapp/feature/Home/controller/home_controller.dart';
import 'package:vaionapp/route/route.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  final _homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: warnabackground,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 25,),
              const Headerwidget(),
              const SizedBox(height: 15,),
              const Searchwidget(),
              const SizedBox(height: 25,),
              Padding(
                padding: const EdgeInsets.only(left: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Datang Langsung Ke toko',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 20, 
                      fontWeight: FontWeight.bold
                      ),
                    )
                  ],
                ),
              ),
              Slidercard(homeController: _homeController),
              const SizedBox(height: 20,),
              CategoryWidget(homeController: _homeController,),
              const SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Favorit',
                    style: ConstTextStyle.stylepoppin(
                      fontsize: 24,
                      fontWeight: FontWeight.bold,
                      color: warnaputih
                      ),
                    ),
                    const SizedBox(height: 10,),
                    SizedBox(
                      height: 168,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: _homeController.favoriteList.length,
                        itemBuilder: (content, index) {
                          final data = _homeController.favoriteList[index];
                          return InkWell(
                            onTap: () {
                              final jsonEncode = data.tojson();
                              Get.toNamed(Routename.detailScreen, arguments: {
                                'data': jsonEncode,
                            });
                            }, 
                            child: FavoriteCard(
                            imageUrl: data.imageUrl,
                            harga: data.harga,
                            label: data.label,
                            rating: data.rating,
                            ),
                          );
                        },
                      ), 
                    ),
                    const SizedBox(
                      height: 20,
                      ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),          
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  Text('Rekomendasi',
                    style: ConstTextStyle.stylepoppin(
                        fontsize: 24,
                        fontWeight: FontWeight.bold,
                        color: warnaputih
                        ),
                      ),
                    const SizedBox(
                      height: 20,
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: _homeController.rekomendasiList.length,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        final data = _homeController.rekomendasiList[index];
                        return CardRekomendasi(
                        imageUrl: data.imageUrl, 
                        label: data.label, 
                        rating: data.rating, 
                        harga: data.harga, 
                        tag: data.tag,
                        );
                      } 
                    )
                  ],
                ),                   
              ),
            ],
          ),
        ),
      )
    );
  }
}

