class KartuuntukmuModel {
  final String restoName;
  final String menutitle;
  final String alamat;
  final bool isDiscount;
  final String discount;
  final String imageUrl;
  final String rating;

  KartuuntukmuModel({
    required this.restoName, 
    required this.menutitle, 
    required this.alamat, 
    required this.isDiscount, 
    required this.discount, 
    required this.imageUrl, 
    required this.rating, 
  });
}