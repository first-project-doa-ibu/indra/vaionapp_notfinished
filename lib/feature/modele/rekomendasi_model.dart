class ProductModel{
  final String imageUrl;
  final String label;
  final String rating;
  final String harga;
  final String tag;

  ProductModel(
    {
    required this.imageUrl, 
    required this.label, 
    required this.rating, 
    required this.harga, 
    required this.tag, 
    }
  );

  factory ProductModel.fromjson(Map<String, dynamic> json) =>ProductModel(
    imageUrl: json['imageUrl'], 
    label: json['label'], 
    rating: json['rating'], 
    harga: json['harga'], 
    tag: json['tag'],
    );

    Map<String, dynamic> tojson(){
      return {
        'imageUrl': imageUrl,
        'label': label,
        'harga': harga,
        'rating': rating,
        'tag': tag,
      };
    }

    ProductModel copyWith({
      String? imageUrl,
      String? label,
      String? rating,
      String? harga,
      String? tag,
    }){
      return ProductModel(
        imageUrl: imageUrl??this.imageUrl, 
        label: label??this.label, 
        rating: rating??this.rating, 
        harga: harga??this.harga, 
        tag: tag??this.tag,
      );
    }
}