class FavoritModel {
  final String restoName;
  final String menutitle;
  final String alamat;
  final bool isFavorit;
  final String harga;
  final String imageUrl;
  final String rating;

  FavoritModel({
    required this.restoName, 
    required this.menutitle, 
    required this.alamat, 
    required this.isFavorit, 
    required this.harga, 
    required this.imageUrl, 
    required this.rating, 
  });

  FavoritModel copywith({
    String? imageUrl,
    String? restoName,
    String? rating,
    String? menutitle,
    String? alamat,
    String? harga,
    bool? isFavorit,
  }) {
    return FavoritModel(
      restoName: restoName??this.restoName, 
      menutitle: menutitle??this.menutitle, 
      alamat: alamat??this.alamat, 
      isFavorit: isFavorit??this.isFavorit, 
      harga: harga??this.harga, 
      imageUrl: imageUrl??this.imageUrl, 
      rating: rating??this.rating);
  }
}