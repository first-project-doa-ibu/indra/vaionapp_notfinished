import 'package:get/get.dart';
import 'package:vaionapp/feature/Detail/Screen/detailScreen.dart';
import 'package:vaionapp/feature/Detail/bindings/Detail_binding.dart';
import 'package:vaionapp/feature/main/bindings/main_biding.dart';
import 'package:vaionapp/feature/main/screen/main_screen.dart';
import 'package:vaionapp/feature/modele/rekomendasi_model.dart';
import 'package:vaionapp/feature/onboard/screen/onboardscreen.dart';
import 'package:vaionapp/feature/order/Screen/OrderScreen.dart';
import 'package:vaionapp/feature/signin/Screen/signin_screen.dart';
import 'package:vaionapp/feature/signup/Screen/signup_screen.dart';
import 'package:vaionapp/feature/splash/splash_screen.dart';
import 'package:vaionapp/route/route.dart';

class Routepages {
  List<GetPage<dynamic>> routes = [
    GetPage(name: Routename.splashScreen, page: () => const SplashScreen(),),
    GetPage(name: Routename.onboardingScreen, page: () => OnboardScreen(),),
    GetPage(name: Routename.signupScreen, page: () => const SignupScreen(),),
    GetPage(name: Routename.signinScreen, page: () => const LoginPage(),),
    GetPage(name: Routename.mainScreen, page: () => MainScreen(),
      binding: MainBinding(),
    ),
    GetPage(name: Routename.detailScreen,
    binding: DetailBinding(),
    page: () {
      ProductModel ProductData = ProductModel(
        imageUrl: '', 
        label: '', 
        rating: '', 
        harga: '', 
        tag: ''
        );
      if(Get.arguments != null && Get.arguments is Map){
        final Map<String, dynamic> args = Get.arguments;

        if (args.containsKey('data')){
          ProductData = ProductModel.fromjson(args['data']);
        }
      }
      return DetailScreen(productModel: ProductData,);
      } 
    ),
    GetPage(name: Routename.orderScreen, page: () {
      ProductModel ProductData = ProductModel(
        imageUrl: '', 
        label: '', 
        rating: '', 
        harga: '', 
        tag: ''
        );
        int totalharga = 0;
        int totalitem = 1;
      if(Get.arguments != null && Get.arguments is Map){
        final Map<String, dynamic> args = Get.arguments;

        if (args.containsKey('data')){
          ProductData = ProductModel.fromjson(args['data']);
        }

        if(args.containsKey('totalharga')) {
          totalharga=args['totalharga'];
        }

        if(args.containsKey('totalitem')) {
          totalitem=args['totalitem'];
        }
      }
      return OrderScreen(productModel: ProductData, 
      totalharga: totalharga, 
      totalitem: totalitem,);} 
    ),
  ];
}