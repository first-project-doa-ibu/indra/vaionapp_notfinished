class Routename{
  static const splashScreen = "/splash-screen";
  static const onboardingScreen = "/onboarding-Screen";
  static const signupScreen = "/signup-Screen";
  static const signinScreen = "/signin-Screen";
  static const mainScreen = "/main-Screen";
  static const detailScreen = "/detail-Screen";
  static const orderScreen = "/order-Screen";
}