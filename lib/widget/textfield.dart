import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/ConstEL/color.dart';
import 'package:vaionapp/ConstEL/font.dart';
import 'package:vaionapp/feature/controller/Auth_controller.dart';

class Textformfieldwidget extends StatelessWidget {
  Textformfieldwidget({super.key, required this.hintText, this.Password});

  final String hintText;
  final bool? Password;
  final authC = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          child:Obx(() => TextField(
            obscureText: authC.isObsecure.isTrue && Password != null, 
            style: ConstTextStyle.styleinter(color: warnaputih, fontsize: 10),
            decoration: InputDecoration(
              suffixIcon: Password != null? IconButton(
                  onPressed: () => authC.changeIsObsecure(),
                  icon: const Icon(Icons.visibility,
                  color: warnaputih,
                  ),
                )
              : const SizedBox(),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: const BorderSide(color: warnatombol, width: 1),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: const BorderSide(color: warnatombol, width: 1),
            ),
          hintText: hintText,
          hintStyle: ConstTextStyle.styleinter(color: warnaputih, fontsize: 10),
          filled: true,
        ),
      ),) 
    );
  }
}