import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:vaionapp/route/route.dart';
// import 'package:vaionapp/feature/onboard/screen/onboardscreen.dart';
import 'package:vaionapp/route/route_pages.dart';
// import 'package:vaionapp/feature/splash/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: Routepages().routes,
      initialRoute: Routename.onboardingScreen,
    );
  }
}